<section id="form">
  <!--form-->
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-form">
          <!--login form-->
          <h2>Login to your account</h2>
          <form method="POST">
            <input type="text" placeholder="Username" name="username" /> <input type="password" placeholder="Password" name="password" /> <span> <input type="checkbox" class="checkbox">
              Keep me signed in
            </span>
            <div style="text-align: center;">
              <button type="submit" class="btn btn-default" style="display: inline-block;">Login</button>
              <button style="display: inline-block;" type="button" class="btn btn-default" onclick="window.location.href = 'register.html'">Register</button>
            </div>
          </form>
        </div>
        <!--/login form-->
      </div>
    </div>
  </div>
</section>
<!--/form-->

