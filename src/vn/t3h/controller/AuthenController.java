package vn.t3h.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import vn.t3h.domain.Account;

@Controller
public class AuthenController {

	/** 
	 * Kiem tra dang nhap
	 *   Input: nhap du lieu tu request (không dùng html form)
	 *   URL request: http://localhost:8080/Eshopper/login_by_pathvariable.html/admin/123456
	 *   Output: true | false
	 * @return
	 */
	@RequestMapping("/login_by_pathvariable.html/{username}/{password}")
	public String loginByPathvariable(@PathVariable ("username") String username, 
			@PathVariable(name="password", required = false, value="123456" ) String password) {
		if (username.equalsIgnoreCase(password)) {
			System.out.println("Login successful.");
		} else {
			System.out.println("Login fail.");
		}
		
		return "";
	}
	/**
	 * URL Request: http://localhost:8080/Eshopper/login_by_param.html?username=admin&password=123456
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("login_by_param.html")
	public String loginByParameter(String username, String password){
		System.out.println("username = " + username);
		System.out.println("password = " + password);
		
		
		return "";
	}
	
	@RequestMapping(value = "login.html", method = RequestMethod.GET)
	public String login() {
		return "login"; // Kết hợp với ViewResolver -> thêm prefix và suffix -> /WEB-INF/pages/login.jsp
	}
	
//	@RequestMapping(value = "login.html", method = RequestMethod.POST)
//	public String authen(String username, String password) {
//		if (username.equalsIgnoreCase(password)) {
//			return "redirect:/index.html";
//		} 
//		return "login";
//	}
	
	@RequestMapping(value = "login.html", method = RequestMethod.POST)
	public String authen(Account account, HttpServletRequest request) {
		
		if (account.getUsername().equalsIgnoreCase(account.getPassword())) {
			return "redirect:/index.html";
		} 
		ModelMap map = new ModelMap();
		map.addAttribute("msgError", "Username or password is incorrect.");
		
		return "login";
	}
}
