package vn.t3h.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class HomeController { // POJO = plain old java object => dung Annotation

	@RequestMapping("/index.html")
	public String index() {
		return "index";  // pages/index.jsp
	}
}
