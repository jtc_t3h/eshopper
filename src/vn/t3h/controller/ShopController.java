package vn.t3h.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ShopController {

	@RequestMapping("/product.html")
	public String product() {
		return "shop.product.list";
	}
}
