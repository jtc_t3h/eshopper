package vn.t3h.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/user")
public class UserController {

	@RequestMapping(value="/list.html", method = RequestMethod.GET)  // http://localhost:8080/Eshopper/user/list.html
	public String list() {
		return "";
	}
	
	@RequestMapping(value= {"/add.html", "/edit.html"}, method = RequestMethod.POST)  //// http://localhost:8080/Eshopper/user/add.html
	public String update() {
		return "";
	}
}
